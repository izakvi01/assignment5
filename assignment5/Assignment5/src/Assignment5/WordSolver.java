package Assignment5;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class WordSolver {
	
	public static List<String> solutionList = new ArrayList<String>();
	public static List<String> nonoList = new ArrayList<String>();

	
	/**
	 * this is the main recursive function
	 * 
	 * if start is equal to end we have found the last step of the 
	 * ladder this is the base case if this is true then we end the recursion
	 * we check the dictionary for potential matches
	 * put all matches in an arrayList then sort the arrayList
	 * if no Potential matches are found we hit a dead end and we have to backtrack
	 * we then go all potential matches and plug them into our recursive makeLadder function
	 * 
	 * 
	 * @param start starting word
	 * @param end  String ending word
	 * @param change int character that changed
	 * @param dictionary  ArrayList<String>
	 * @param no // meaningless
	 * @return List<String> solution list
	 */
	public List<String> MakeLadder(String start, String end, int change, ArrayList<String> dictionary, ArrayList<String> no) throws IllegalWordException
		{
		
		if(!(dictionary.contains(start)))
		{
			throw new IllegalWordException(start + " is not in the dictionary.\n");
		}
		
		//add the current start value to the ladder solution
		solutionList.add(start);
//		if (solutionList.size() > 200) { 
//			solutionList.remove(start);
//			return solutionList;
//			
//		}
		
		//if start equals end, we have found the last step. Let's print it!
		if (start.equals(end)) 
		{
//			for (String s : solutionList) {
//				System.out.println(s);
//			}
			return solutionList;
		}
		
		//split the starting word into characters
		ArrayList<Character> startChars = getChars(start);
		
		ArrayList<String> potentialMatches = new ArrayList<String>();
		
		Iterator<String> itDictionary = dictionary.iterator();
		
		//for every word in the dictionary, check to see if it is a potential path along the ladder from the starting word
		while (itDictionary.hasNext())
		{
			String dictionaryWord = itDictionary.next();
			
			if (dictionaryWord.length() == start.length()) 
			{
		//every possible potential solution is placed inside the potentialMatches ArrayList
				checkForPotentialSolution(start, end, change, startChars, potentialMatches, dictionaryWord, no);
			}
		}
		
		//sort the potentialMatches ArrayList so we check the words closer to "end" first
		Collections.sort(potentialMatches);
		
		//if there are no potentialMatches, we hit a dead end in our ladder. We have to backtrack.
		if (potentialMatches.isEmpty()) 
		{
			solutionList.remove(start);
			return solutionList;
		}
		
		//let's go through our potentialMatches and plug them back into the recursive MakeLadder function
		Iterator<String> itPotentialMatches = potentialMatches.iterator();
		while (itPotentialMatches.hasNext()) 
		{
		
		//take out the number attached to the potentialMatch that indicates how close to "end" the word is
			String currentPotentialMatch = itPotentialMatches.next().substring(1);
	//		System.out.println(currentPotentialMatch);
			
			ArrayList<Character> potentialMatchChars = getChars(currentPotentialMatch);
			
		//this small function and while loop will indicate what character was changed in the word, 
		//so we can update the "change" value in our recursive call
			Iterator<Character> itMatch = potentialMatchChars.iterator();
			Iterator<Character> itStart = startChars.iterator();
			int position = 1;
			while (itMatch.hasNext()) 
			{
				
				if ( !( itMatch.next().equals(itStart.next()) ) ) 
				{
					break;
				}
				position++;
			}
			if (MakeLadder(currentPotentialMatch, end, position, dictionary, no).contains(currentPotentialMatch)) 
			{
				return solutionList;
			}
			else {
				nonoList.add(currentPotentialMatch);
				no.add(currentPotentialMatch);
			}
//			if (MakeLadder(currentPotentialMatch, end, position, dictionary)) {
//				return solutionList;
//			}
		}
		
		//if this is not the correct solution, we have to remove the start value because it was wrong
		solutionList.remove(start);
		return solutionList;
	}
	
	
	/**
	 * checks for potential solutions
	 * checks to see if the words are the same length
	 * checks to see if the dictionary word and the start word
	 * are 1 character apart
	 *
	 * @param start // starting word
	 * @param end  // ending word
	 * @param change  // int describing what position changed so we dont loop infinitely
	 * @param startChars //characters of starting word
	 * @param potentialMatches //list of potential matches
	 * @param dictionaryWord //word from the dictionary that we are checking
	 * @param no // meaningless
	 */
	private static void checkForPotentialSolution(String start, String end, int change,
			ArrayList<Character> startChars,
			ArrayList<String> potentialMatches, String dictionaryWord, ArrayList<String>no) 
	{
		
		//if the dictionary word and the start word are NOT the same length, there's no point in checking for potential matches
		if (dictionaryWord.length() == start.length()) 
		{
		
			ArrayList<Character> dictionaryWordChars = getChars(dictionaryWord);
		
		//check to see if the dictionary word and the start word are 1 character apart
			Iterator<Character> itDictionaryWord = dictionaryWordChars.iterator();
			Iterator<Character> itStart = startChars.iterator();
			int differences = 0;
			int position = 1;
			while (itDictionaryWord.hasNext()) 
			{
				if ( !( itDictionaryWord.next().equals(itStart.next()) )  ) 
				{
		
		//this arbitrary value is used to make sure we aren't changing the same position over and over again
					if (position == change) 
					{
						differences = 5;
					}
					differences++;
				}
				position++;
			}
			
		//if this is indeed a valid potentialMatch, we check how close it is to "end" and add it to our arrayList
			if ((differences < 2) && !(solutionList.contains(dictionaryWord)) && !(nonoList.contains(dictionaryWord)) && !(no.contains(dictionaryWord)) ) 
			{
				
				Iterator<Character> itDictionaryWord2 = dictionaryWordChars.iterator();
				ArrayList<Character> endChars = getChars(end);
				Iterator<Character> itEnd = endChars.iterator();
				int differences2 = 0;
				while (itDictionaryWord2.hasNext()) {
					if ( !( itDictionaryWord2.next().equals(itEnd.next()) ) ) 
					{
						differences2++;
					}
				}
				potentialMatches.add(differences2 + dictionaryWord);
			}	
		}
	}

	
	
	/**
	 * this method splits up words into characters
	 * 
	 * @param start
	 * @return ArrayList<Character> // the starting word as a char array
	 */
	private static ArrayList<Character> getChars(String start) 
	{
		ArrayList<Character> startChars = new ArrayList<Character>();
		for (int i = 0; i < start.length(); i++){
			startChars.add((Character) start.charAt(i));
		}
		return startChars;
	}


}
	