/**
 * This program creates a word ladder between two words.
 * The user will give the program a start and an end word 
 * if the words are the same length the program will print a list 
 * of words connecting the two words together
 * Every consecutive word will only differ by one letter. 
 * If no ladder exists or the words are of different lengths a message 
 * will be printed on the screen.
 * 
 * @date 11/14/2014
 * @author David Emelianov, Rohith Kolli, Ishaq Zakvi
 * @version 1.00001
 */
package Assignment5;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

/**
 *  
 * If a ladder is found it is printed on the screen. 
 * The amount of time it took to make the ladder is also printed. 
 * Methods called in this class include: 
 * computeLadder, and validateResult from the wordLadderSolver class,
 * start, stop, reset, and getTime from the StopWatch class
 * 
 * noSuchLadderException is caught
 * if thrown by the computeLadder method
 * 
 * @param args // the dictionary file and the input words list file    
 */
public class Assign5Driver

{
    public static void main(String[] args)
    {
    	
    	String word1;
    	String word2;
    	String line;
    	String inputFile = "input.txt";
        // Create a word ladder solver object
        Assignment5Interface wordLadderSolver = new WordLadderSolver();

//		try {
//			 BufferedReader input = new BufferedReader(new FileReader(inputFile));
//		} catch (FileNotFoundException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//    	
        
        try
        {
        	BufferedReader input = new BufferedReader(new FileReader(inputFile));
    		while ((line = input.readLine()) != null)
    		{
        
        
        // Read file in
        // for each line in the file call compute ladder
    			try {
        	
    		

    			    String text[] = line.split("[ ]");
    			    word1 = text[0];
    		        
    			  //  word1 = "spaghett";
    			    word2 = text[text.length - 1];
    		        
    		        System.out.println("Word 1 is " + word1);
    		        System.out.println("Word 2 is " + word2 + "\n");
    		        
    		        StopWatch sWatch = new StopWatch();
    		        sWatch.start();
    		        List<String> result = wordLadderSolver.computeLadder(word1,word2);
    	            sWatch.stop();
    	            
    	             boolean correct = wordLadderSolver.validateResult(word1, word2, result);
    	          
    	            if (correct) 
    	            {
    	            	for (String s : result) {
    	    				System.out.println(s);
    	    		
    	            	}
    	           }
    	            else {
    	            	System.out.println("No ladder was found between " + word1 + " and " + word2 + ".");
    	        //    	System.out.println("It took " + sWatch.getElapsedTime() + " ns to attempt to find a ladder from " + word1 + " to "+ word2 + ".");
    	            }
    	            System.out.println("\nIt took " + sWatch.getElapsedTime() + " ns to search for a ladder from " + word1 + " to "+ word2 + ".");
    	            sWatch.reset();
    	            System.out.println("**********\n");
    	            
    	            ((WordLadderSolver) wordLadderSolver).clearSolutions();
 
    				} 
    				catch (NoSuchLadderException e) 
    				{
    					System.out.println(e.getMessage());
    				} 
    		}
    		input.close();
    	}
        catch (IOException e) 
        {
			e.printStackTrace();
		}
        
    }
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          