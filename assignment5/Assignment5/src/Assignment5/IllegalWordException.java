package Assignment5;
/**
 * 
 * Exception to be thrown when the starting word is not in the dictionary
 * 
 */
public class IllegalWordException extends Exception
{
    private static final long serialVersionUID = 1L;

    public IllegalWordException(String message)
    {
        super(message);
    }

    public IllegalWordException(String message, Throwable throwable)
    {
        super(message, throwable);
    }
}
