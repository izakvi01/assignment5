package Assignment5;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

//do not change class name or interface it implements

/**
 * 
 * This class implements Assignment5Interface
 * The dictionary is populated, 
 * makeLadder is called from the WordSolver class
 * and validate result determines there is a ladder between the two words
 * 
 * @author David Emelianov, Rohith Kolli, Ishaq Zakvi
 */
public class WordLadderSolver implements Assignment5Interface
{
 // delcare class members here.
	
	ArrayList<String> dictionary = new ArrayList<String>();
	
	List<String> solutionList = new ArrayList<String>();
	
	WordSolver wordSolver = new WordSolver();
	
 // add a constructor for this object. HINT: it would be a good idea to set up the dictionary there
	public WordLadderSolver() 
	{
		String fileName = "assn5words.dat";
		String line;
		
		try {
		
		BufferedReader input = new BufferedReader(new FileReader(fileName));
			
		while ((line = input.readLine()) != null)
		{
			
			if(line.contains("*"))
			{
			
			    String parts[] = line.split("[*]");
			    line = parts[0];
				dictionary.add(line);
			}
			if(line.contains("+"))
			{
			    String parts[] = line.split("[+]");
			    line = parts[0];
				dictionary.add(line);
			}	
			
			if(line.contains(","))
			{
			    String parts[] = line.split("[,]");
			    line = parts[0];
				dictionary.add(line);
			}
			
			if(line.contains(" "))
			{
			    String parts[] = line.split("[ ]");
			    line = parts[0];
				dictionary.add(line);

			}
			
			dictionary.add(line);
		}
	
		
		input.close();
		
		} 
		catch (IOException e) 
		{
			System.out.println("\nData file not found, check if " + fileName + " is in the program directory");
		}		
		
		//test 
		/*int sz = dictionary.size();
		for(int i = 0; i<sz; i++)
		{
			System.out.println(dictionary.get(i).toString());
		}*/
				
	}

 // do not change signature of the method implemented from the interface
 @Override
 /**
  * this method calls MakeLadder from the WordSolver
  * class and puts the result
  * in the List<String> solutionList
  * 
  * @Overrides compute ladder
  * @param String startword  // beggining of ladder
  * @param String endWord    // end of ladder
  * @return List<String> containing the solution ladder of words from starting word to ending word
  */
 public List<String> computeLadder(String startWord, String endWord) throws NoSuchLadderException 
 {
      try
	 {
    	   ArrayList<String> no = new ArrayList<String>();
    	   no.clear();
    	  
	 solutionList = wordSolver.MakeLadder(startWord, endWord, 0, dictionary, no);
	 if(solutionList.isEmpty())
		{
			throw new NoSuchLadderException("There is no ladder connecting " + startWord + " to " + endWord + ".\n**********");
		}
	 	 
	 }
	 catch (IllegalWordException e)
	 {
		 System.out.println(e.getMessage());
		 
	 }
		
		 return solutionList;
 }
	
	 
	
 
 public void clearSolutions() 
 {
	 solutionList.clear();
 }

 @Override

 /**
  * method that checks to see if a ladder was found from startingWord 
  * to endingWord
  * 
  * @overrides validateResult
  * @param String startingWord // start of ladder
  * @param String endingWord  // end of ladder 
  * @param List<Sting> wordLadder // a list containing the ladder from startingword to ending word
  * @returns boolean based on whether the ending word is on the List<String>
  * if the ending word is on the List<Sting> then a solution was found and true is returned. 
  * otherwise false is returned
  */
 public boolean validateResult(String startWord, String endWord, List<String> wordLadder) 
 {
     if (wordLadder.contains(endWord)) {
    	 return true;
     }
     else 
     { 
    	 return false; 
    	 }
 }


 
}
